====================================
	MYSQL - CULMINATING ACTIVITY
====================================


-- 1.) Return the customerName of the customers who are from the Philippines.
SELECT customerName FROM customers WHERE country = "Philippines";

--2.) Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";


--3.) Return the product name and MSRP of the product named "The Titanic"
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";


--4.) Return the first and last name of the employee whose email is "jfirreli@classicmodelcars.com"
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";


--5.) Return the names of customers who have no registered state
SELECT customerName, state FROM customers WHERE state IS NULL;

--6.) Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";


--7.) Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit FROM customers
WHERE country != "USA" AND creditLimit > 3000;


--8.) Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";


--9.) Return the product lines whose text description mentions the phrase 'state of the art'
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";


--10.) Return the countries of customers without duplication
SELECT DISTINCT country FROM customers; 


--11.) Return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;


--12.) Return the customer names and countries of customers whose country is USA, france, or canada
SELECT customerName, country FROM customers WHERE country IN ("USA","France","Canada");


--13.) Return the first name, last name, and office's city of employees whose offices are in tokyo
SELECT firstName, lastName, city FROM employees JOIN offices ON employees.officeCode = offices.officeCode WHERE city = "Tokyo";


--14.) Return the customer names of customers who were served by the employee named "Leslie Thompson"
SELECT customerName FROM customers 
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE firstName = 'Leslie' AND lastName = 'Thompson';

--15) Return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT productName, customerName FROM customers 
JOIN orders ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products on orderdetails.productCode = products.productCode WHERE customerName LIKE "%Baane Mini Imports%";

--16.) Return the employees first names, last names, customer names and offices countries of transactions whose customers and offices are in the same country
SELECT firstName, lastName, customerName FROM customers 
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE country = "USA";


--17.) Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;


--18.) Return the customer's name with a phone number "+81"
SELECT customerName, phone FROM customers WHERE phone LIKE "+81%";





/Stretch Goal/
1. -- Return the product name of the orders where customer name is Baane Mini Imports
2. --  Return the last name and first name of employees that reports to Anthony Bow
3. -- Return the product name of the product with the maximum MSRP
4. -- Return the number of products group by productline
5. -- Return the number of producs where the status is cancelled



===========ANSWERS:===============

1.)

SELECT productName FROM customers 
JOIN orders ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products on orderdetails.productCode = products.productCode WHERE customerName LIKE "%Baane Mini Imports%";


2.)

SELECT lastName, firstName from employees WHERE reportsTo = 1143;

3.) 

SELECT productName, MSRP FROM products GROUP BY MSRP DESC LIMIT 1;

4.) 
SELECT count(DISTINCT productName) AS COUNT_productname, productLine FROM products GROUP BY productLine;

5.)
SELECT count(*) AS COUNT_orderNumber FROM orders WHERE status = "Cancelled";

























